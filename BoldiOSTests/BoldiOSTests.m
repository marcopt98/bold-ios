//
//  BoldiOSTests.m
//  BoldiOSTests
//
//  Created by Marco André Marinho Lopes on 03/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Constants.h"

#define TestNeedsToWaitForBlock() __block BOOL blockFinished = NO
#define BlockFinished() blockFinished = YES
#define WaitForBlock() while (CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0, true) && !blockFinished)

@interface BoldiOSTests : XCTestCase

@property (strong, nonatomic) NSMutableArray *albumsList;

@end

@implementation BoldiOSTests


- (void)testGetAlbumsList {
    [self checkAlbumList:@"1"];
}

- (void)testGetImageForAlbum {
    [self checkAlbumList:@"1"];
    [self getImagesService];
}

-(void) getImagesService{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    __block Albums *thisAlbum;
    __block PhotosFromAlbum *thisPhotoAlbum;
    
    TestNeedsToWaitForBlock();
    
    dispatch_async(queue, ^{
        dispatch_semaphore_t test = dispatch_semaphore_create(0);
        for (Albums* thisAlbum in self.albumsList) {
            [Services getAlbumsPhotoWithID:[thisAlbum.albumID stringValue] completionHandler:^(NSMutableArray *thePhotosAlbum) {
                
                thisPhotoAlbum = (PhotosFromAlbum *)[[PhotosFromAlbum alloc]initWithDictionary:[thePhotosAlbum firstObject]];
                
                thisAlbum.thumbnailUrl = thisPhotoAlbum.thumbnailUrl;
                
                dispatch_semaphore_signal(test);
            }];
            dispatch_semaphore_wait(test, DISPATCH_TIME_FOREVER);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            BlockFinished();
            
            //Fails because the two strings are not equally on porpuse
            
            XCTAssertTrue([thisAlbum.thumbnailUrl isEqualToString:thisPhotoAlbum.url],
                          @"Strings are not equal %@ %@", thisAlbum.thumbnailUrl, thisPhotoAlbum.url);
        });
        
        
    });
    
    WaitForBlock();
}


- (void)checkAlbumList:(NSString *)thisID  {
    TestNeedsToWaitForBlock();
    
    [Services getAlbumsWithID:thisID completionHandler:^(NSMutableArray *theAlbums) {
        
        self.albumsList = [[NSMutableArray alloc] init];
        
        for (NSDictionary *item in theAlbums) {
            [self.albumsList addObject: (Albums *)[[Albums alloc]initWithDictionary:item]];
        }
        
        Albums *thisAlbum = self.albumsList[0];
        
        XCTAssertTrue([[thisAlbum.userID stringValue] isEqualToString:thisID]);
        
        BlockFinished();
        
    }];
    
    WaitForBlock();
}


@end
