//
//  Posts.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "Posts.h"
#import "Constants.h"

@implementation Posts

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        
        self.userID = [dictionary objectForKey:@"userId"];
        self.postID = [dictionary objectForKey:@"id"];
        self.title = [dictionary objectForKey:@"title"];
        self.body = [dictionary objectForKey:@"body"];
    }
    return self;
}

@end
