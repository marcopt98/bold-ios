//
//  Users.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Users : NSObject

@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *username;
@property (strong,nonatomic) NSString *email;
@property (strong,nonatomic) NSNumber *userID;


-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

