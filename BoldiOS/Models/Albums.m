//
//  Albums.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "Albums.h"
#import "Constants.h"

@implementation Albums

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        
        self.userID = [dictionary objectForKey:@"userId"];
        self.albumID = [dictionary objectForKey:@"id"];
        self.title = [dictionary objectForKey:@"title"];
    }
    return self;
}

@end
