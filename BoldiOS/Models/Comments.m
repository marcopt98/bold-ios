//
//  Comments.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "Comments.h"
#import "Constants.h"

@implementation Comments

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        
        self.commentID = [dictionary objectForKey:@"id"];
        self.postID = [dictionary objectForKey:@"postId"];
        self.name = [dictionary objectForKey:@"name"];
        self.email = [dictionary objectForKey:@"email"];
        self.body = [dictionary objectForKey:@"body"];
    }
    return self;
}

@end
