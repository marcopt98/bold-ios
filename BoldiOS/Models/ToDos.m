//
//  ToDos.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "ToDos.h"
#import "Constants.h"

@implementation ToDos

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        
        self.userID = [dictionary objectForKey:@"userId"];
        self.todoID = [dictionary objectForKey:@"id"];
        self.title = [dictionary objectForKey:@"title"];
        self.completed = [[dictionary objectForKey:@"completed"] boolValue];
        
    }
    return self;
}

@end
