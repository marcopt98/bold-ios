//
//  PhotosFromAlbum.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "PhotosFromAlbum.h"

@implementation PhotosFromAlbum

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        
        self.albumID = [dictionary objectForKey:@"albumId"];
        self.photoID = [dictionary objectForKey:@"id"];
        self.title = [dictionary objectForKey:@"title"];
        self.url = [dictionary objectForKey:@"url"];
        self.thumbnailUrl = [dictionary objectForKey:@"thumbnailUrl"];
        
    }
    return self;
}

@end
