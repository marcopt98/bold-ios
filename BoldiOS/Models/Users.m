//
//  Users.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "Users.h"
#import "Constants.h"

@implementation Users

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self = [super init];
    if (self) {
        
        self.userID = [dictionary objectForKey:@"id"];
        self.username = [dictionary objectForKey:@"username"];
        self.name = [dictionary objectForKey:@"name"];
        self.email = [dictionary objectForKey:@"email"];
        
    }
    return self;
}

@end
