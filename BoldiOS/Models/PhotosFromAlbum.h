//
//  PhotosFromAlbum.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotosFromAlbum : NSObject

@property (strong, nonatomic) NSNumber *albumID;
@property (strong, nonatomic) NSNumber *photoID;
@property(strong,nonatomic) NSString *title;
@property(strong,nonatomic) NSString *url;
@property(strong,nonatomic) NSString *thumbnailUrl;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

