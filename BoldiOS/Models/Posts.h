//
//  Posts.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Posts : NSObject

@property(strong,nonatomic) NSString *title;
@property(strong,nonatomic) NSString *body;
@property (strong, nonatomic) NSNumber *userID;
@property (strong, nonatomic) NSNumber *postID;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

