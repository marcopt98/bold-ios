//
//  Albums.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Albums : NSObject

@property (strong,nonatomic) NSString *title;
@property (strong, nonatomic) NSNumber *userID;
@property (strong, nonatomic) NSNumber *albumID;
@property (strong,nonatomic) NSString *thumbnailUrl;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
