//
//  Comments.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comments : NSObject

@property(strong,nonatomic) NSString *name;
@property(strong,nonatomic) NSString *email;
@property(strong,nonatomic) NSString *body;
@property (strong, nonatomic) NSNumber *commentID;
@property (strong, nonatomic) NSNumber *postID;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

