//
//  ToDos.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToDos : NSObject

@property (strong,nonatomic) NSString *title;
@property (strong,nonatomic) NSNumber *userID;
@property (strong,nonatomic) NSNumber *todoID;
@property (nonatomic) BOOL completed;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end

