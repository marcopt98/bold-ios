//
//  AppDelegate.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 03/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

