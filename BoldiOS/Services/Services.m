//
//  Services.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "Services.h"
#import "Constants.h"

@implementation Services

+(void) getUsers:(void (^)(NSMutableArray *))completion{
    
    [ConnectionManager getItens:getUsersLink completionHandler:^(id result) {
        if (result) {
            completion((NSMutableArray *) result);
        }else{
            completion(nil);
        }
    }];
}

+(void) getTodosWithID:(NSString *)userID completionHandler:(void (^)(NSMutableArray *))completion{
    
    NSString *finalURL;
    finalURL = [getUserToDosLink stringByReplacingOccurrencesOfString:@"{userID}" withString:userID];
    
    [ConnectionManager getItens:finalURL completionHandler:^(id result) {
        if (result) {
            completion((NSMutableArray *) result);
        }else{
            completion(nil);
        }
    }];
}


+(void) getAlbumsWithID:(NSString *)userID completionHandler:(void (^)(NSMutableArray *))completion{
    
    NSString *finalURL;
    finalURL = [getUserAlbunsLink stringByReplacingOccurrencesOfString:@"{userID}" withString:userID];
    
    [ConnectionManager getItens:finalURL completionHandler:^(id result) {
        if (result) {
            completion((NSMutableArray *) result);
        }else{
            completion(nil);
        }
    }];
}

+(void) getAlbumsPhotoWithID:(NSString *)albumID completionHandler:(void (^)(NSMutableArray *))completion{
    
    NSString *finalURL;
    finalURL = [getAlbumPhotosLink stringByReplacingOccurrencesOfString:@"{albumID}" withString:albumID];
    
    [ConnectionManager getItens:finalURL completionHandler:^(id result) {
        if (result) {
            completion((NSMutableArray *) result);
        }else{
            completion(nil);
        }
    }];
}


+(void) getPostsWithID:(NSString *)userID completionHandler:(void (^)(NSMutableArray *))completion{
    
    NSString *finalURL;
    finalURL = [getUserPostsLink stringByReplacingOccurrencesOfString:@"{userID}" withString:userID];
    
    [ConnectionManager getItens:finalURL completionHandler:^(id result) {
        if (result) {
            completion((NSMutableArray *) result);
        }else{
            completion(nil);
        }
    }];
}


+(void) getCommentsWithID:(NSString *)postID completionHandler:(void (^)(NSMutableArray *))completion{
    
    NSString *finalURL;
    finalURL = [getPostCommentsLink stringByReplacingOccurrencesOfString:@"{postID}" withString:postID];
    
    [ConnectionManager getItens:finalURL completionHandler:^(id result) {
        if (result) {
            completion((NSMutableArray *) result);
        }else{
            completion(nil);
        }
    }];
}

@end
