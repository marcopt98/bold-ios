//
//  Services.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Services : NSObject

+(void) getUsers:(void (^)(NSMutableArray *))completion;
+(void) getTodosWithID:(NSString *)userID completionHandler:(void (^)(NSMutableArray *))completion;
+(void) getAlbumsWithID:(NSString *)userID completionHandler:(void (^)(NSMutableArray *))completion;
+(void) getAlbumsPhotoWithID:(NSString *)albumID completionHandler:(void (^)(NSMutableArray *))completion;
+(void) getPostsWithID:(NSString *)userID completionHandler:(void (^)(NSMutableArray *))completion;
+(void) getCommentsWithID:(NSString *)userID completionHandler:(void (^)(NSMutableArray *))completion;
@end
