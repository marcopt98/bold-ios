//
//  ConnectionManager.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManager : NSObject

+(void)getItens:(NSString *)url completionHandler:(void(^)(id result))completion;

@end
