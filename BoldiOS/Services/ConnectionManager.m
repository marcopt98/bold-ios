//
//  ConnectionManager.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "ConnectionManager.h"
#import "AFNetworking.h"

@implementation ConnectionManager

+(void) getItens:(NSString *)url completionHandler:(void (^)(id))completion{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        completion(responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completion(nil);
    }];
}

@end
