//
//  Constants.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Services.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "ConnectionManager.h"
#import "Users.h"
#import "ToDos.h"
#import "Posts.h"
#import "Comments.h"
#import "Albums.h"
#import "PhotosFromAlbum.h"



static NSString* const getUsersLink = @"http://jsonplaceholder.typicode.com/users";
static NSString* const getUserToDosLink = @"http://jsonplaceholder.typicode.com/todos?userId={userID}";
static NSString* const getUserPostsLink = @"http://jsonplaceholder.typicode.com/posts?userId={userID}";
static NSString* const getPostCommentsLink = @"http://jsonplaceholder.typicode.com/comments?postId={postID}";
static NSString* const getUserAlbunsLink = @"http://jsonplaceholder.typicode.com/albums?userId={userID}";
static NSString* const getAlbumPhotosLink = @"http://jsonplaceholder.typicode.com/photos?albumId={albumID}";



