//
//  AlbumsPhotosCollectionViewController.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 06/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "AlbumsPhotosCollectionViewController.h"
#import "AlbumsPhotosCollectionViewCell.h"

@interface AlbumsPhotosCollectionViewController ()

@property (strong, nonatomic) NSMutableArray *photosList;
@property (nonatomic) BOOL *isOneColumn;

@end

@implementation AlbumsPhotosCollectionViewController

static NSString * const reuseIdentifier = @"AlbumsPhotoCustomCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self serviceCall];
}

#pragma mark - UI

-(void) setupUI{
    self.navigationItem.title = @"Album Photos";
    
    UIBarButtonItem *changeCollecitonViewOptions = [[UIBarButtonItem alloc]
                                   initWithImage: [UIImage imageNamed:@"edit_view"]
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(changeView:)];
    self.navigationItem.rightBarButtonItem = changeCollecitonViewOptions;
}

-(IBAction)changeView:(id)sender {
    self.isOneColumn = !self.isOneColumn;
    [self.collectionView reloadData];
}

#pragma mark - Services

-(void) serviceCall{
    [Services getAlbumsPhotoWithID:[self.thisAlbum.albumID stringValue] completionHandler:^(NSMutableArray *thePhotosAlbum) {
        
        self.photosList = [[NSMutableArray alloc] init];
        
        for (NSDictionary *item in thePhotosAlbum) {
            [self.photosList addObject: (PhotosFromAlbum *)[[PhotosFromAlbum alloc]initWithDictionary:item]];
        }
        
        [self.collectionView reloadData];
    }];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photosList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AlbumsPhotosCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    PhotosFromAlbum *thisPhoto = self.photosList[indexPath.row];
    
    NSURL *url = [NSURL URLWithString:thisPhoto.url];
    [cell.albumImageView setImageWithURL:url];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isOneColumn) {
        return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height/2);
    }else{
        return CGSizeMake(120, 120);
    }
}


@end
