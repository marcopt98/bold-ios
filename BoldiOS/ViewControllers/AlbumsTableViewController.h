//
//  AlbumsTableViewController.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 06/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface AlbumsTableViewController : UITableViewController

@property (strong, nonatomic) Users *thisUser;

@end

