//
//  AlbumsTableViewController.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 06/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "AlbumsTableViewController.h"
#import "AlbumsTableViewCell.h"
#import "AlbumsPhotosCollectionViewController.h"

@interface AlbumsTableViewController ()

@property (strong, nonatomic) NSMutableArray *albumsList;

@end

@implementation AlbumsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self serviceCall];
}


#pragma mark - UI

-(void) setupUI{
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Albums", self.thisUser.name];
}


#pragma mark - Services

-(void) serviceCall{
    [Services getAlbumsWithID:[self.thisUser.userID stringValue] completionHandler:^(NSMutableArray *theAlbums) {
        
        self.albumsList = [[NSMutableArray alloc] init];
        
        for (NSDictionary *item in theAlbums) {
            [self.albumsList addObject: (Albums *)[[Albums alloc]initWithDictionary:item]];
        }
        
        [self getImagesService];
    }];
}


-(void) getImagesService{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        dispatch_semaphore_t thisSemaphore = dispatch_semaphore_create(0);
        
        for (Albums *thisAlbum in self.albumsList) {
            [Services getAlbumsPhotoWithID:[thisAlbum.albumID stringValue] completionHandler:^(NSMutableArray *thePhotosAlbum) {
                
                PhotosFromAlbum *thisPhotoAlbum = (PhotosFromAlbum *)[[PhotosFromAlbum alloc]initWithDictionary:[thePhotosAlbum firstObject]];
                
                thisAlbum.thumbnailUrl = thisPhotoAlbum.thumbnailUrl;
                
                dispatch_semaphore_signal(thisSemaphore);
            }];
            
            dispatch_semaphore_wait(thisSemaphore, DISPATCH_TIME_FOREVER);
            
            //Faz reload à table no fim de cada pedido, mas mostra logo informação ao utilizador
            //            dispatch_async(dispatch_get_main_queue(), ^{
            //                [self.tableView reloadData];
            //
            //            });
        }
        
        //Faz reload à table no fim dos pedidos todos, demora uns segundos mais a mostrar informação ao utilizador, mas mostra logo tudo, e só é chamado uma vez...
        //Uma otima ideia aqui seria inserir um Loading...
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];

        });
    
    });
                   
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.albumsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlbumsTableViewCell *albumsCell = (AlbumsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"AlbumsCustomCell"];
    
    if (albumsCell == nil) {
        albumsCell = [[[NSBundle mainBundle] loadNibNamed:@"AlbumsTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    Albums *thisAlbum = self.albumsList[indexPath.row];
    
    NSURL *url = [NSURL URLWithString:thisAlbum.thumbnailUrl];
    
    [albumsCell.albumImage setImageWithURL:url];
    albumsCell.albumTitle.text = thisAlbum.title;
    
    return albumsCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AlbumsPhotosCollectionViewController *vc = (AlbumsPhotosCollectionViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AlbumsPhotosCollectionViewControllerID"];

    vc.thisAlbum = self.albumsList[indexPath.row];

    CATransition* transition = [CATransition animation];
    transition.duration = 0.33;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];

    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

    [self.navigationController pushViewController:vc animated:YES];
}

@end
