//
//  CommentsTableViewController.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 06/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "CommentsTableViewController.h"
#import "CommentsTableViewCell.h"

@interface CommentsTableViewController ()

@property (strong, nonatomic) NSMutableArray *commentsList;

@end

@implementation CommentsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self serviceCall];
}

#pragma mark - UI

-(void) setupUI{
    self.navigationItem.title = @"Comment's Post";
}


#pragma mark - Services

-(void) serviceCall{
    [Services getCommentsWithID:[self.thisPost.postID stringValue] completionHandler:^(NSMutableArray *theComments) {
        
        self.commentsList = [[NSMutableArray alloc] init];

        for (NSDictionary *item in theComments) {
            [self.commentsList addObject: (Comments *)[[Comments alloc]initWithDictionary:item]];
        }
        
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.commentsList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 100.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 100)];
    
    UITextView *TextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 100)];
    [TextView setFont:[UIFont boldSystemFontOfSize:15]];
    [TextView setEditable:NO];
    TextView.text = self.thisPost.body;
    
    [view addSubview:TextView];
    [TextView setBackgroundColor:[UIColor colorWithRed:211/255.0 green:211/255.0 blue:211/255.0 alpha:1.0]];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentsTableViewCell *commentCell = (CommentsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CommentsCustomCell"];
    
    if (commentCell == nil) {
        commentCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    Comments *thisComment = self.commentsList[indexPath.row];
    
    commentCell.userImageView.image = [UIImage imageNamed:@"user_placeHolder"];
    commentCell.userEmail.text = thisComment.email;
    commentCell.userComment.text = thisComment.body;
    
    return commentCell;
}


@end
