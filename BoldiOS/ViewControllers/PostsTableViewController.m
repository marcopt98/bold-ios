//
//  PostsTableViewController.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 06/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "PostsTableViewController.h"
#import "PostsTableViewCell.h"
#import "CommentsTableViewController.h"

@interface PostsTableViewController ()

@property (strong, nonatomic) NSMutableArray *postsList;

@end

@implementation PostsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self serviceCall];
}

#pragma mark - UI

-(void) setupUI{
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Posts", self.thisUser.name];
}

#pragma mark - Services

-(void) serviceCall{
    [Services getPostsWithID:[self.thisUser.userID stringValue] completionHandler:^(NSMutableArray *thePosts) {
        
        self.postsList = [[NSMutableArray alloc] init];
        
        for (NSDictionary *item in thePosts) {
            [self.postsList addObject: (Posts *)[[Posts alloc]initWithDictionary:item]];
        }
        
        
        [self.tableView reloadData];
    }];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.postsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PostsTableViewCell *postsCell = (PostsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PostsCustomCell"];
    
    if (postsCell == nil) {
        postsCell = [[[NSBundle mainBundle] loadNibNamed:@"PostsTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    Posts *thisPost = self.postsList[indexPath.row];
    
    postsCell.postTitle.text = thisPost.title;
    postsCell.postBody.text = thisPost.body;
    
    return postsCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CommentsTableViewController *vc = (CommentsTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"CommentsTableViewControllerID"];
    
    vc.thisPost = self.postsList[indexPath.row];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.33;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
