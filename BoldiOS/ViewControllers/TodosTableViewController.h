//
//  TodosTableViewController.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface TodosTableViewController : UITableViewController

@property (strong, nonatomic) Users *thisUser;

@end

