//
//  UsersTableViewController.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "UsersTableViewController.h"
#import "UsersTableViewCell.h"
#import "Users.h"
#import "Constants.h"
#import "ChooseActionViewController.h"

@interface UsersTableViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *usersList;

@end

@implementation UsersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self serviceCall];
}


#pragma mark - UI

-(void) setupUI{
    self.navigationItem.title = @"Bold iOS Challenge";
}


#pragma mark - Services

-(void) serviceCall{
    [Services getUsers:^(NSMutableArray *theUsers) {
        
        self.usersList = [[NSMutableArray alloc] init];
        
        for (NSDictionary *item in theUsers) {
            [self.usersList addObject: (Users *)[[Users alloc]initWithDictionary:item]];
        }
        
        [self.tableView reloadData];
    }];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.usersList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UsersTableViewCell *userCell = (UsersTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"UsersCustomCell"];
    
    if (userCell == nil) {
        userCell = [[[NSBundle mainBundle] loadNibNamed:@"UsersTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    Users *thisUser = self.usersList[indexPath.row];
    
    userCell.userImage.image = [UIImage imageNamed:@"user_placeHolder"];
    userCell.userName.text = thisUser.name;
    userCell.userEmail.text = thisUser.email;
    
    return userCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        
    ChooseActionViewController *vc = (ChooseActionViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ChooseActionViewControllerID"];

    vc.thisUser = self.usersList[indexPath.row];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.33;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.navigationController pushViewController:vc animated:YES];
}


@end
