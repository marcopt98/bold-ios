//
//  ChooseActionViewController.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "ChooseActionViewController.h"
#import "TodosTableViewController.h"
#import "PostsTableViewController.h"
#import "AlbumsTableViewController.h"

@interface ChooseActionViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *albumsButton;
@property (weak, nonatomic) IBOutlet UIButton *postsButton;
@property (weak, nonatomic) IBOutlet UIButton *todosButton;

@end

@implementation ChooseActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
}

#pragma mark - UI

- (void)setupUI {
    self.userImageView.image = [UIImage imageNamed:@"user_placeHolder"];
    self.nameLabel.text = self.thisUser.name;
    
    [self.albumsButton setTitle:@"Albums" forState:UIControlStateNormal];
    [self.albumsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.albumsButton setBackgroundColor:[UIColor colorWithRed:0.15 green:0.50 blue:0.80 alpha:1.0]];
    
    [self.postsButton setTitle:@"Posts" forState:UIControlStateNormal];
    [self.postsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.postsButton setBackgroundColor:[UIColor colorWithRed:0.15 green:0.50 blue:0.80 alpha:1.0]];
    
    [self.todosButton setTitle:@"To Do's" forState:UIControlStateNormal];
    [self.todosButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.todosButton setBackgroundColor:[UIColor colorWithRed:0.15 green:0.50 blue:0.80 alpha:1.0]];
    
    self.navigationItem.title = @"Bold iOS Challenge";
    self.navigationController.navigationItem.title = @"Back";
}


#pragma mark - Button Actions

- (IBAction)onClickAlbumsButton:(UIButton *)sender {
    AlbumsTableViewController *vc = (AlbumsTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"AlbumsTableViewControllerID"];

    vc.thisUser = self.thisUser;

    CATransition* transition = [CATransition animation];
    transition.duration = 0.33;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];

    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onClickPostsButton:(UIButton *)sender {
    PostsTableViewController *vc = (PostsTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PostsTableViewControllerID"];
    
    vc.thisUser = self.thisUser;
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.33;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onClickTodosButton:(UIButton *)sender {
    TodosTableViewController *vc = (TodosTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"TodosTableViewControllerID"];

    vc.thisUser = self.thisUser;

    CATransition* transition = [CATransition animation];
    transition.duration = 0.33;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];

    [self.navigationController pushViewController:vc animated:YES];
}


@end
