//
//  TodosTableViewController.m
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 05/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import "TodosTableViewController.h"
#import "Constants.h"
#import "TodosTableViewCell.h"

@interface TodosTableViewController ()

@property (strong, nonatomic) NSMutableArray *todosList;

@end

@implementation TodosTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupUI];
    [self serviceCall];
}

#pragma mark - UI

-(void) setupUI{
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Posts", self.thisUser.name];
}

#pragma mark - Services

-(void) serviceCall{
    [Services getTodosWithID:[self.thisUser.userID stringValue] completionHandler:^(NSMutableArray *theTodos) {
        
        self.todosList = [[NSMutableArray alloc] init];
        
        for (NSDictionary *item in theTodos) {
            [self.todosList addObject: (ToDos *)[[ToDos alloc]initWithDictionary:item]];
        }
        
        [self.tableView reloadData];
    }];
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.todosList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TodosTableViewCell *todoCell = (TodosTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"TodosCustomCell"];
    
    if (todoCell == nil) {
        todoCell = [[[NSBundle mainBundle] loadNibNamed:@"TodosTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    ToDos *thisTodo = self.todosList[indexPath.row];
    
    todoCell.title.text = thisTodo.title;
    
    if (thisTodo.completed) {
        todoCell.customImageView.image = [UIImage imageNamed:@"status_completed"];
    }else{
        todoCell.customImageView.image = [UIImage imageNamed:@"status_incompleted"];
    }
    
    
    return todoCell;
}

@end
