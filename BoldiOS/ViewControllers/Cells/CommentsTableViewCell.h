//
//  CommentsTableViewCell.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 06/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userEmail;
@property (weak, nonatomic) IBOutlet UILabel *userComment;

@end

