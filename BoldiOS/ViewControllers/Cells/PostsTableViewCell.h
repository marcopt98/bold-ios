//
//  PostsTableViewCell.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 06/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *postTitle;
@property (weak, nonatomic) IBOutlet UITextView *postBody;

@end

