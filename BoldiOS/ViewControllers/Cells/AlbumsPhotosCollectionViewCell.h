//
//  AlbumsPhotosCollectionViewCell.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 06/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumsPhotosCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;

@end

