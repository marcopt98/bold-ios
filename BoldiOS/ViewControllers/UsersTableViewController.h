//
//  UsersTableViewController.h
//  BoldiOS
//
//  Created by Marco André Marinho Lopes on 04/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UsersTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
