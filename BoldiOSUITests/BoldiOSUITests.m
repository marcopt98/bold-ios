//
//  BoldiOSUITests.m
//  BoldiOSUITests
//
//  Created by Marco André Marinho Lopes on 03/07/2019.
//  Copyright © 2019 Marco. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface BoldiOSUITests : XCTestCase

@end

@implementation BoldiOSUITests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.

    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;

    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testAlbumsPhotos {

    XCUIApplication *app = [[XCUIApplication alloc] init];
    XCUIElementQuery *tablesQuery = app.tables;
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Sincere@april.biz"]/*[[".cells.staticTexts[@\"Sincere@april.biz\"]",".staticTexts[@\"Sincere@april.biz\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    [app.buttons[@"Albums"] tap];
    sleep(3);
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"non esse culpa molestiae omnis sed optio"]/*[[".cells.staticTexts[@\"non esse culpa molestiae omnis sed optio\"]",".staticTexts[@\"non esse culpa molestiae omnis sed optio\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ swipeUp];
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"eaque aut omnis a"]/*[[".cells.staticTexts[@\"eaque aut omnis a\"]",".staticTexts[@\"eaque aut omnis a\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    [[[[[[app.otherElements containingType:XCUIElementTypeNavigationBar identifier:@"Album Photos"] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeCollectionView].element swipeUp];
    
    XCUIElement *albumPhotosNavigationBar = app.navigationBars[@"Album Photos"];
    XCUIElement *editViewButton = albumPhotosNavigationBar.buttons[@"edit view"];
    [editViewButton tap];
    [[[[app.collectionViews childrenMatchingType:XCUIElementTypeCell] elementBoundByIndex:1] childrenMatchingType:XCUIElementTypeOther].element swipeUp];
    [editViewButton tap];
    
    [albumPhotosNavigationBar.buttons[@"Leanne Graham Albums"] tap];
    [app.navigationBars[@"Leanne Graham Albums"].buttons[@"Bold iOS Challenge"] tap];
    [app.navigationBars[@"Bold iOS Challenge"].buttons[@"Bold iOS Challenge"] tap];
    
}

- (void)testPosts {
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    XCUIElementQuery *tablesQuery = app.tables;
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Patricia Lebsack"]/*[[".cells.staticTexts[@\"Patricia Lebsack\"]",".staticTexts[@\"Patricia Lebsack\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    [app.buttons[@"Posts"] tap];
    [[[tablesQuery.cells containingType:XCUIElementTypeStaticText identifier:@"qui explicabo molestiae dolorem"] childrenMatchingType:XCUIElementTypeTextView].element swipeUp];
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"magnam ut rerum iure"]/*[[".cells.staticTexts[@\"magnam ut rerum iure\"]",".staticTexts[@\"magnam ut rerum iure\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    
    XCUIElement *gabrielleJadaCoUkStaticText = tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Gabrielle@jada.co.uk"]/*[[".cells.staticTexts[@\"Gabrielle@jada.co.uk\"]",".staticTexts[@\"Gabrielle@jada.co.uk\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/;
    [gabrielleJadaCoUkStaticText swipeUp];
    [gabrielleJadaCoUkStaticText swipeDown];
    [app.navigationBars[@"Comment's Post"].buttons[@"Patricia Lebsack Posts"] tap];
    [app.navigationBars[@"Patricia Lebsack Posts"].buttons[@"Bold iOS Challenge"] tap];
    [app.navigationBars[@"Bold iOS Challenge"].buttons[@"Bold iOS Challenge"] tap];
    
}

- (void)testToDos {
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    XCUIElementQuery *tablesQuery = app.tables;
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Chelsey Dietrich"]/*[[".cells.staticTexts[@\"Chelsey Dietrich\"]",".staticTexts[@\"Chelsey Dietrich\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ swipeUp];
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Glenna Reichert"]/*[[".cells.staticTexts[@\"Glenna Reichert\"]",".staticTexts[@\"Glenna Reichert\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    [app.buttons[@"To Do's"] tap];
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"quos quo possimus suscipit minima ut"]/*[[".cells.staticTexts[@\"quos quo possimus suscipit minima ut\"]",".staticTexts[@\"quos quo possimus suscipit minima ut\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ swipeUp];
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"et provident amet rerum consectetur et voluptatum"]/*[[".cells.staticTexts[@\"et provident amet rerum consectetur et voluptatum\"]",".staticTexts[@\"et provident amet rerum consectetur et voluptatum\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ swipeUp];
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"laudantium eius officia perferendis provident perspiciatis asperiores"]/*[[".cells.staticTexts[@\"laudantium eius officia perferendis provident perspiciatis asperiores\"]",".staticTexts[@\"laudantium eius officia perferendis provident perspiciatis asperiores\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ swipeUp];
    [app.navigationBars[@"Glenna Reichert Posts"].buttons[@"Bold iOS Challenge"] tap];
    [app.navigationBars[@"Bold iOS Challenge"].buttons[@"Bold iOS Challenge"] tap];
    
            
}


@end
